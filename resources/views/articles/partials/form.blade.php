@csrf
<div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" id="title" class="form-control" value="{{ $article->title }}">
    {!! $errors->first('title', '<div class="text-danger mt-1">:message</div>') !!}
</div>
<div class="form-group">
    <label for="content">Content</label>
    <textarea name="content" id="content" class="form-control">{{ $article->content }}</textarea>
    {!! $errors->first('content', '<div class="text-danger mt-1">:message</div>') !!}
</div>
<button type="submit" class="btn btn-danger">{{ $submit }}</button>